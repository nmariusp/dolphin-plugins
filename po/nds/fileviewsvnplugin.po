# translation of fileviewsvnplugin.po to Low Saxon
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Manfred Wiese <m.j.wiese@web.de>, 2010, 2011.
#
msgid ""
msgstr ""
"Project-Id-Version: fileviewsvnplugin\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-09 01:39+0000\n"
"PO-Revision-Date: 2011-01-12 10:06+0100\n"
"Last-Translator: Manfred Wiese <m.j.wiese@web.de>\n"
"Language-Team: Low Saxon <kde-i18n-nds@kde.org>\n"
"Language: nds\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.0\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: fileviewsvnplugin.cpp:71
#, kde-format
msgctxt "@item:inmenu"
msgid "SVN Update"
msgstr "SVN-Archiev opfrischen"

#: fileviewsvnplugin.cpp:77
#, kde-format
msgctxt "@item:inmenu"
msgid "Show Local SVN Changes"
msgstr "Lokaal SVN-Ännern wiesen"

#: fileviewsvnplugin.cpp:83
#, kde-format
msgctxt "@item:inmenu"
msgid "SVN Commit..."
msgstr "Na SVN-Archiev inspelen..."

#: fileviewsvnplugin.cpp:89
#, kde-format
msgctxt "@item:inmenu"
msgid "SVN Add"
msgstr "Na SVN-Archiev tofögen"

#: fileviewsvnplugin.cpp:95
#, kde-format
msgctxt "@item:inmenu"
msgid "SVN Delete"
msgstr "Ut SVN-Archiev wegmaken"

#: fileviewsvnplugin.cpp:101
#, fuzzy, kde-format
#| msgctxt "@item:inmenu"
#| msgid "SVN Delete"
msgctxt "@item:inmenu"
msgid "SVN Revert"
msgstr "Ut SVN-Archiev wegmaken"

#: fileviewsvnplugin.cpp:107
#, kde-format
msgctxt "@item:inmenu"
msgid "Show SVN Updates"
msgstr "SVN-Opfrischen wiesen"

#: fileviewsvnplugin.cpp:115
#, fuzzy, kde-format
#| msgctxt "@item:inmenu"
#| msgid "SVN Commit..."
msgctxt "@action:inmenu"
msgid "SVN Log..."
msgstr "Na SVN-Archiev inspelen..."

#: fileviewsvnplugin.cpp:120
#, fuzzy, kde-format
#| msgctxt "@item:inmenu"
#| msgid "SVN Commit..."
msgctxt "@action:inmenu"
msgid "SVN Checkout..."
msgstr "Na SVN-Archiev inspelen..."

#: fileviewsvnplugin.cpp:125
#, fuzzy, kde-format
#| msgctxt "@item:inmenu"
#| msgid "SVN Commit..."
msgctxt "@action:inmenu"
msgid "SVN Cleanup..."
msgstr "Na SVN-Archiev inspelen..."

#: fileviewsvnplugin.cpp:222
#, kde-format
msgctxt "@info:status"
msgid "SVN status update failed. Disabling Option \"Show SVN Updates\"."
msgstr ""
"SVN-Status lett sik nich opfrischen. Funkschoon \"SVN-Opfrischen wiesen\" "
"warrt utmaakt."

#: fileviewsvnplugin.cpp:358
#, fuzzy, kde-format
#| msgctxt "@item:inmenu"
#| msgid "SVN Update"
msgctxt "@title:window"
msgid "SVN Update"
msgstr "SVN-Archiev opfrischen"

#: fileviewsvnplugin.cpp:362
#, kde-format
msgctxt "@info:status"
msgid "Updating SVN repository..."
msgstr "SVN-Archiev warrt opfrischt..."

#: fileviewsvnplugin.cpp:363
#, kde-format
msgctxt "@info:status"
msgid "Update of SVN repository failed."
msgstr "Opfrischen vun't SVN-Archiev is fehlslaan."

#: fileviewsvnplugin.cpp:364
#, kde-format
msgctxt "@info:status"
msgid "Updated SVN repository."
msgstr "SVN-Archiev opfrischt."

#: fileviewsvnplugin.cpp:378
#, fuzzy, kde-format
#| msgctxt "@item:inmenu"
#| msgid "Show Local SVN Changes"
msgctxt "@info:status"
msgid "Could not show local SVN changes."
msgstr "Lokaal SVN-Ännern wiesen"

#: fileviewsvnplugin.cpp:393
#, fuzzy, kde-format
#| msgctxt "@info:status"
#| msgid "Commit of SVN changes failed."
msgctxt "@info:status"
msgid "Could not show local SVN changes: svn diff failed."
msgstr "Inspelen vun SVN-Ännern is fehlslaan."

#: fileviewsvnplugin.cpp:405 fileviewsvnplugin.cpp:589
#: fileviewsvnplugin.cpp:618
#, kde-format
msgctxt "@info:status"
msgid "Could not show local SVN changes: could not start kompare."
msgstr ""

#: fileviewsvnplugin.cpp:437
#, kde-format
msgctxt "@info:status"
msgid "Adding files to SVN repository..."
msgstr "Dateien warrt dat SVN-Archiev toföögt..."

#: fileviewsvnplugin.cpp:438
#, kde-format
msgctxt "@info:status"
msgid "Adding of files to SVN repository failed."
msgstr "Tofögen vun Dateien na't SVN-Archiev is fehlslaan."

#: fileviewsvnplugin.cpp:439
#, kde-format
msgctxt "@info:status"
msgid "Added files to SVN repository."
msgstr "Dateien na't SVN-Archiev toföögt."

#: fileviewsvnplugin.cpp:445
#, kde-format
msgctxt "@info:status"
msgid "Removing files from SVN repository..."
msgstr "Dateien warrt ut dat SVN-Archiev wegmaakt..."

#: fileviewsvnplugin.cpp:446
#, kde-format
msgctxt "@info:status"
msgid "Removing of files from SVN repository failed."
msgstr "Wegmaken vun Dateien ut dat SVN-Archiev is fehlslaan."

#: fileviewsvnplugin.cpp:447
#, kde-format
msgctxt "@info:status"
msgid "Removed files from SVN repository."
msgstr "Dateien ut dat SVN-Archiev wegmaakt."

#: fileviewsvnplugin.cpp:467 fileviewsvnplugin.cpp:553
#, fuzzy, kde-format
#| msgctxt "@item:inmenu"
#| msgid "SVN Delete"
msgctxt "@title:window"
msgid "SVN Revert"
msgstr "Ut SVN-Archiev wegmaken"

#: fileviewsvnplugin.cpp:471
#, fuzzy, kde-format
#| msgctxt "@info:status"
#| msgid "Removing files from SVN repository..."
msgctxt "@info:status"
msgid "Reverting files from SVN repository..."
msgstr "Dateien warrt ut dat SVN-Archiev wegmaakt..."

#: fileviewsvnplugin.cpp:472
#, fuzzy, kde-format
#| msgctxt "@info:status"
#| msgid "Removing of files from SVN repository failed."
msgctxt "@info:status"
msgid "Reverting of files from SVN repository failed."
msgstr "Wegmaken vun Dateien ut dat SVN-Archiev is fehlslaan."

#: fileviewsvnplugin.cpp:473
#, fuzzy, kde-format
#| msgctxt "@info:status"
#| msgid "Removed files from SVN repository."
msgctxt "@info:status"
msgid "Reverted files from SVN repository."
msgstr "Dateien ut dat SVN-Archiev wegmaakt."

#: fileviewsvnplugin.cpp:557
#, kde-format
msgctxt "@info:status"
msgid "Reverting changes to file..."
msgstr ""

#: fileviewsvnplugin.cpp:558
#, kde-format
msgctxt "@info:status"
msgid "Revert file failed."
msgstr ""

#: fileviewsvnplugin.cpp:559
#, kde-format
msgctxt "@info:status"
msgid "File reverted."
msgstr ""

#: fileviewsvnplugin.cpp:576 fileviewsvnplugin.cpp:599
#: fileviewsvnplugin.cpp:604
#, fuzzy, kde-format
#| msgctxt "@info:status"
#| msgid "Commit of SVN changes failed."
msgctxt "@info:status"
msgid "Could not show local SVN changes for a file: could not get file."
msgstr "Inspelen vun SVN-Ännern is fehlslaan."

#: fileviewsvnplugin.cpp:645 fileviewsvnplugin.cpp:668
#, kde-format
msgctxt "@info:status"
msgid "Commit of SVN changes failed."
msgstr "Inspelen vun SVN-Ännern is fehlslaan."

#: fileviewsvnplugin.cpp:663 svncommitdialog.cpp:154
#, kde-format
msgctxt "@title:window"
msgid "SVN Commit"
msgstr "Na SVN-Archiev inspelen"

#: fileviewsvnplugin.cpp:667
#, kde-format
msgctxt "@info:status"
msgid "Committing SVN changes..."
msgstr "SVN-Ännern warrt inspeelt..."

#: fileviewsvnplugin.cpp:669
#, kde-format
msgctxt "@info:status"
msgid "Committed SVN changes."
msgstr "SVN-Ännern inspeelt."

#. i18n: ectx: label, entry (showUpdates), group (General)
#: fileviewsvnpluginsettings.kcfg:9
#, kde-format
msgid "Show updates"
msgstr "Opfrischen wiesen"

#: svncheckoutdialog.cpp:62
#, kde-format
msgctxt "@title:window"
msgid "Choose a directory to checkout"
msgstr ""

#: svncheckoutdialog.cpp:108
#, kde-format
msgctxt "@info:status"
msgid "SVN checkout: checkout in process..."
msgstr ""

#: svncheckoutdialog.cpp:111
#, kde-format
msgctxt "@info:status"
msgid "SVN checkout: checkout failed."
msgstr ""

#: svncheckoutdialog.cpp:113
#, kde-format
msgctxt "@info:status"
msgid "SVN checkout: checkout successful."
msgstr ""

#. i18n: ectx: property (windowTitle), widget (QWidget, SvnCheckoutDialog)
#: svncheckoutdialog.ui:20
#, fuzzy, kde-format
#| msgctxt "@item:inmenu"
#| msgid "SVN Commit..."
msgid "SVN Checkout"
msgstr "Na SVN-Archiev inspelen..."

#. i18n: ectx: property (text), widget (QLabel, label)
#: svncheckoutdialog.ui:26
#, fuzzy, kde-format
#| msgctxt "@info:status"
#| msgid "Updated SVN repository."
msgid "URL of repository:"
msgstr "SVN-Archiev opfrischt."

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: svncheckoutdialog.ui:36
#, kde-format
msgid "Checkout directory:"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, cbOmitExternals)
#: svncheckoutdialog.ui:46
#, kde-format
msgid "Omit externals"
msgstr ""

#. i18n: ectx: property (text), widget (QPushButton, pbOk)
#. i18n: ectx: property (text), widget (QPushButton, buttonOk)
#: svncheckoutdialog.ui:71 svncleanupdialog.ui:80 svnlogdialog.ui:146
#: svnprogressdialog.ui:46
#, kde-format
msgid "OK"
msgstr ""

#. i18n: ectx: property (text), widget (QPushButton, pbCancel)
#. i18n: ectx: property (text), widget (QPushButton, buttonCancel)
#: svncheckoutdialog.ui:81 svncleanupdialog.ui:96 svnprogressdialog.ui:33
#, kde-format
msgid "Cancel"
msgstr ""

#: svncleanupdialog.cpp:26
#, kde-format
msgctxt "@title:window"
msgid "Choose a directory to clean up"
msgstr ""

#: svncleanupdialog.cpp:60
#, kde-format
msgctxt "@info:status"
msgid "SVN clean up completed successfully."
msgstr ""

#: svncleanupdialog.cpp:62
#, kde-format
msgctxt "@info:status"
msgid "SVN clean up failed for %1"
msgstr ""

#. i18n: ectx: property (windowTitle), widget (QWidget, SvnCleanupDialog)
#: svncleanupdialog.ui:20
#, fuzzy, kde-format
#| msgctxt "@item:inmenu"
#| msgid "SVN Commit..."
msgid "SVN Cleanup..."
msgstr "Na SVN-Archiev inspelen..."

#. i18n: ectx: property (text), widget (QLabel, label)
#: svncleanupdialog.ui:26
#, kde-format
msgid "Clean up directory:"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, checkBoxUnversioned)
#: svncleanupdialog.ui:43
#, kde-format
msgid "Delete unversioned files and directories"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, checkBoxIgnored)
#: svncleanupdialog.ui:50
#, kde-format
msgid "Delete ignored files and directories"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, checkBoxExternals)
#: svncleanupdialog.ui:57
#, kde-format
msgid "Include externals"
msgstr ""

#: svncommitdialog.cpp:88
#, kde-format
msgctxt "@label"
msgid "Description:"
msgstr "Beschrieven:"

#: svncommitdialog.cpp:103
#, kde-format
msgctxt "@action:button"
msgid "Refresh"
msgstr ""

#: svncommitdialog.cpp:108
#, kde-format
msgctxt "@action:button"
msgid "Commit"
msgstr "Inspelen"

#: svncommitdialog.cpp:114
#, fuzzy, kde-format
#| msgctxt "@item:inmenu"
#| msgid "SVN Delete"
msgctxt "@item:inmenu"
msgid "Revert"
msgstr "Ut SVN-Archiev wegmaken"

#: svncommitdialog.cpp:121
#, fuzzy, kde-format
#| msgctxt "@item:inmenu"
#| msgid "Show Local SVN Changes"
msgctxt "@item:inmenu"
msgid "Show changes"
msgstr "Lokaal SVN-Ännern wiesen"

#: svncommitdialog.cpp:128
#, kde-format
msgctxt "@item:inmenu"
msgid "Add file"
msgstr ""

#: svncommitdialog.cpp:156
#, kde-format
msgctxt "@title:column"
msgid "Path"
msgstr ""

#: svncommitdialog.cpp:157
#, kde-format
msgctxt "@title:column"
msgid "Status"
msgstr ""

#: svncommitdialog.cpp:210
#, kde-format
msgctxt "@item:intable"
msgid "Unversioned"
msgstr ""

#: svncommitdialog.cpp:213
#, kde-format
msgctxt "@item:intable"
msgid "Modified"
msgstr ""

#: svncommitdialog.cpp:216
#, kde-format
msgctxt "@item:intable"
msgid "Added"
msgstr ""

#: svncommitdialog.cpp:219
#, fuzzy, kde-format
#| msgctxt "@item:inmenu"
#| msgid "SVN Delete"
msgctxt "@item:intable"
msgid "Deleted"
msgstr "Ut SVN-Archiev wegmaken"

#: svncommitdialog.cpp:222
#, kde-format
msgctxt "@item:intable"
msgid "Conflict"
msgstr ""

#: svncommitdialog.cpp:225
#, kde-format
msgctxt "@item:intable"
msgid "Missing"
msgstr ""

#: svncommitdialog.cpp:228
#, kde-format
msgctxt "@item:intable"
msgid "Update required"
msgstr ""

#: svnlogdialog.cpp:94
#, fuzzy, kde-format
#| msgctxt "@info:status"
#| msgid "Updated SVN repository."
msgid "Update to revision"
msgstr "SVN-Archiev opfrischt."

#: svnlogdialog.cpp:98 svnlogdialog.cpp:116
#, kde-format
msgid "Revert to revision"
msgstr ""

#: svnlogdialog.cpp:102
#, fuzzy, kde-format
#| msgctxt "@item:inmenu"
#| msgid "Show Local SVN Changes"
msgid "Show changes"
msgstr "Lokaal SVN-Ännern wiesen"

#: svnlogdialog.cpp:109
#, kde-format
msgid "Changes against working copy"
msgstr ""

#: svnlogdialog.cpp:271
#, fuzzy, kde-format
#| msgctxt "@info:status"
#| msgid "Update of SVN repository failed."
msgctxt "@info:status"
msgid "SVN log: update to revision failed."
msgstr "Opfrischen vun't SVN-Archiev is fehlslaan."

#: svnlogdialog.cpp:273
#, kde-format
msgctxt "@info:status"
msgid "SVN log: update to revision %1 successful."
msgstr ""

#: svnlogdialog.cpp:283 svnlogdialog.cpp:294
#, kde-format
msgctxt "@info:status"
msgid "SVN log: revert to revision failed."
msgstr ""

#: svnlogdialog.cpp:285 svnlogdialog.cpp:296
#, kde-format
msgctxt "@info:status"
msgid "SVN log: revert to revision %1 successful."
msgstr ""

#. i18n: ectx: property (windowTitle), widget (QWidget, SvnLogDialog)
#: svnlogdialog.ui:20
#, fuzzy, kde-format
#| msgctxt "@item:inmenu"
#| msgid "SVN Commit..."
msgid "SVN Log"
msgstr "Na SVN-Archiev inspelen..."

#. i18n: ectx: property (text), widget (QTableWidget, tLog)
#: svnlogdialog.ui:55
#, kde-format
msgid "Revision"
msgstr ""

#. i18n: ectx: property (text), widget (QTableWidget, tLog)
#: svnlogdialog.ui:60
#, kde-format
msgid "Author"
msgstr ""

#. i18n: ectx: property (text), widget (QTableWidget, tLog)
#: svnlogdialog.ui:65
#, kde-format
msgid "Date"
msgstr ""

#. i18n: ectx: property (text), widget (QTableWidget, tLog)
#: svnlogdialog.ui:70
#, kde-format
msgid "Message"
msgstr ""

#. i18n: ectx: property (text), widget (QPushButton, pbNext100)
#: svnlogdialog.ui:104
#, kde-format
msgid "Next 100"
msgstr ""

#. i18n: ectx: property (text), widget (QPushButton, pbRefresh)
#: svnlogdialog.ui:114
#, kde-format
msgid "Refresh"
msgstr ""

#: svnprogressdialog.cpp:53
#, kde-format
msgctxt "@info:status"
msgid "Error starting: %1"
msgstr ""
