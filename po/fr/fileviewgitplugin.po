# translation of fileviewgitplugin.po to Français
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Joëlle Cornavin <jcorn@free.fr>, 2010, 2012.
# xavier <xavier.besnard@neuf.fr>, 2013.
# Simon Depiets <sdepiets@gmail.com>, 2017, 2018.
#
msgid ""
msgstr ""
"Project-Id-Version: fileviewgitplugin\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-09 01:39+0000\n"
"PO-Revision-Date: 2018-02-12 09:38+0800\n"
"Last-Translator: Simon Depiets <sdepiets@gmail.com>\n"
"Language-Team: French <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 2.0\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: checkoutdialog.cpp:30
#, kde-kuit-format
msgctxt "@title:window"
msgid "<application>Git</application> Checkout"
msgstr "Extraction de <application>Git</application>"

#: checkoutdialog.cpp:41
#, kde-format
msgctxt "@action:button"
msgid "Checkout"
msgstr "Extraction"

#: checkoutdialog.cpp:54
#, kde-format
msgctxt "@option:radio Git Checkout"
msgid "Branch:"
msgstr "Branche :"

#: checkoutdialog.cpp:61
#, kde-format
msgctxt "@option:radio Git Checkout"
msgid "Tag:"
msgstr "Étiquette :"

#: checkoutdialog.cpp:71 pushdialog.cpp:80
#, kde-format
msgctxt "@title:group"
msgid "Options"
msgstr "Options"

#: checkoutdialog.cpp:76
#, kde-format
msgctxt "@option:check"
msgid "Create New Branch: "
msgstr "Créez une nouvelle branche : "

#: checkoutdialog.cpp:77
#, kde-format
msgctxt "@info:tooltip"
msgid "Create a new branch based on a selected branch or tag."
msgstr ""
"Crée une nouvelle branche reposant sur une branche ou une étiquette "
"sélectionnée."

#: checkoutdialog.cpp:92 pushdialog.cpp:83
#, kde-format
msgctxt "@option:check"
msgid "Force"
msgstr "Forcer"

#: checkoutdialog.cpp:93
#, kde-format
msgctxt "@info:tooltip"
msgid "Discard local changes."
msgstr "Abandonne les modifications locales."

#: checkoutdialog.cpp:125
#, kde-format
msgctxt "@info:tooltip"
msgid "There are no tags in this repository."
msgstr "Il n'y a aucune étiquette dans ce dépôt."

#: checkoutdialog.cpp:185
#, kde-format
msgctxt "@title:group"
msgid "Branch Base"
msgstr "Branche « Base »"

#: checkoutdialog.cpp:186
#, kde-format
msgctxt "@title:group"
msgid "Checkout"
msgstr "Extraction"

#: checkoutdialog.cpp:206
#, kde-format
msgctxt "@info:tooltip"
msgid "You must enter a valid name for the new branch first."
msgstr "Vous devez d'abord saisir un nom valable pour la nouvelle branche."

#: checkoutdialog.cpp:213
#, kde-format
msgctxt "@info:tooltip"
msgid "A branch with the name '%1' already exists."
msgstr "Une branche portant le nom « %1 » existe déjà."

#: checkoutdialog.cpp:220
#, kde-format
msgctxt "@info:tooltip"
msgid "Branch names may not contain any whitespace."
msgstr "Il est impossible d'avoir des blancs dans les noms de branches."

#: checkoutdialog.cpp:227
#, kde-format
msgctxt "@info:tooltip"
msgid "You must select a valid branch first."
msgstr "Vous devez d'abord sélectionner une branche valable."

#: checkoutdialog.cpp:248
#, kde-format
msgctxt ""
"@item:intext Prepended to the current branch name to get the default name "
"for a newly created branch"
msgid "branch"
msgstr "branche"

#: commitdialog.cpp:26
#, kde-kuit-format
msgctxt "@title:window"
msgid "<application>Git</application> Commit"
msgstr "Validation <application>Git</application>"

#: commitdialog.cpp:37
#, kde-format
msgctxt "@action:button"
msgid "Commit"
msgstr "Validation"

#: commitdialog.cpp:46
#, kde-format
msgctxt "@title:group"
msgid "Commit message"
msgstr "Message de validation"

#: commitdialog.cpp:63
#, kde-format
msgctxt "@option:check"
msgid "Amend last commit"
msgstr "Modifier la dernière validation"

#: commitdialog.cpp:69
#, kde-format
msgctxt "@info:tooltip"
msgid "There is nothing to amend."
msgstr "Il n'y a rien à modifier."

#: commitdialog.cpp:76
#, kde-format
msgctxt "@action:button Add Signed-Off line to the message widget"
msgid "Sign off"
msgstr "Approuver"

#: commitdialog.cpp:77
#, kde-format
msgctxt "@info:tooltip"
msgid "Add Signed-off-by line at the end of the commit message."
msgstr "Ajoute une ligne « Approuvé par » à la fin du message de validation."

#: commitdialog.cpp:134
#, kde-format
msgctxt "@info:tooltip"
msgid "You must enter a commit message first."
msgstr "Vous devez d'abord saisir un message de validation."

#: fileviewgitplugin.cpp:49
#, kde-kuit-format
msgid "<application>Git</application> Revert"
msgstr ""
"Retour du contenu des fichiers vers une version donnée de <application>Git</"
"application>"

#: fileviewgitplugin.cpp:55
#, kde-kuit-format
msgid "<application>Git</application> Add"
msgstr "Ajout à <application>Git</application>"

#: fileviewgitplugin.cpp:61
#, kde-kuit-format
msgid "Show Local <application>Git</application> Changes"
msgstr "Afficher les changements locaux <application>Git</application>"

#: fileviewgitplugin.cpp:67
#, kde-kuit-format
msgid "<application>Git</application> Remove"
msgstr "Suppression de <application>Git</application>"

#: fileviewgitplugin.cpp:73
#, kde-kuit-format
msgid "<application>Git</application> Checkout..."
msgstr "Extraction de <application>Git</application>..."

#: fileviewgitplugin.cpp:79
#, kde-kuit-format
msgid "<application>Git</application> Commit..."
msgstr "Validation dans <application>Git</application>..."

#: fileviewgitplugin.cpp:85
#, kde-kuit-format
msgid "<application>Git</application> Create Tag..."
msgstr "Créer une étiquette <application>Git</application>..."

#: fileviewgitplugin.cpp:90
#, kde-kuit-format
msgid "<application>Git</application> Push..."
msgstr "Insertion dans <application>Git</application>..."

#: fileviewgitplugin.cpp:95
#, kde-kuit-format
msgid "<application>Git</application> Pull..."
msgstr "Importation dans <application>Git</application>..."

#: fileviewgitplugin.cpp:100
#, kde-kuit-format
msgid "<application>Git</application> Merge..."
msgstr "Fusion dans <application>Git</application>..."

#: fileviewgitplugin.cpp:104
#, kde-kuit-format
msgid "<application>Git</application> Log..."
msgstr "Historique <application>Git</application>..."

#: fileviewgitplugin.cpp:414
#, kde-kuit-format
msgid "Adding files to <application>Git</application> repository..."
msgstr "Ajout de fichiers au dépôt <application>Git</application>..."

#: fileviewgitplugin.cpp:415
#, kde-kuit-format
msgid "Adding files to <application>Git</application> repository failed."
msgstr "L'ajout de fichiers au dépôt <application>Git</application> a échoué."

#: fileviewgitplugin.cpp:416
#, kde-kuit-format
msgid "Added files to <application>Git</application> repository."
msgstr "Fichiers ajoutés au dépôt <application>Git</application>."

#: fileviewgitplugin.cpp:426
#, kde-kuit-format
msgid "Removing files from <application>Git</application> repository..."
msgstr "Suppression de fichiers du dépôt <application>Git</application>..."

#: fileviewgitplugin.cpp:427
#, kde-kuit-format
msgid "Removing files from <application>Git</application> repository failed."
msgstr ""
"La suppression de fichiers du dépôt <application>Git</application> a échoué."

#: fileviewgitplugin.cpp:428
#, kde-kuit-format
msgid "Removed files from <application>Git</application> repository."
msgstr "Fichiers supprimés du dépôt <application>Git</application>."

#: fileviewgitplugin.cpp:434
#, kde-kuit-format
msgid "Reverting files from <application>Git</application> repository..."
msgstr ""
"Retour du contenu des fichiers du dépôt <application>Git</application> en "
"cours..."

#: fileviewgitplugin.cpp:435
#, kde-kuit-format
msgid "Reverting files from <application>Git</application> repository failed."
msgstr ""
"Le retour du contenu des fichiers du dépôt <application>Git</application> a "
"échoué."

#: fileviewgitplugin.cpp:436
#, kde-kuit-format
msgid "Reverted files from <application>Git</application> repository."
msgstr ""
"Le retour des contenu des fichiers du dépôt <application>SVN</application> a "
"réussi."

#: fileviewgitplugin.cpp:478
#, kde-kuit-format
msgid "<application>Git</application> Log failed."
msgstr "L'affichage de l'historique <application>Git</application> a échoué."

#: fileviewgitplugin.cpp:501
#, kde-kuit-format
msgid "<application>Git</application> Log"
msgstr "Historique <application>Git</application>"

#: fileviewgitplugin.cpp:517
#, kde-format
msgctxt "Git commit hash"
msgid "Commit"
msgstr "Validation"

#: fileviewgitplugin.cpp:518
#, kde-format
msgctxt "Git commit date"
msgid "Date"
msgstr "Date"

#: fileviewgitplugin.cpp:519
#, kde-format
msgctxt "Git commit message"
msgid "Message"
msgstr "Message"

#: fileviewgitplugin.cpp:520
#, kde-format
msgctxt "Git commit author"
msgid "Author"
msgstr "Auteur"

#: fileviewgitplugin.cpp:564
#, kde-kuit-format
msgid "Switched to branch '%1'"
msgstr "Passé dans la branche « %1 »"

#: fileviewgitplugin.cpp:570
#, kde-kuit-format
msgid "HEAD is now at %1"
msgstr "« HEAD » est maintenant à %1"

#: fileviewgitplugin.cpp:574
#, kde-kuit-format
msgid "Switched to a new branch '%1'"
msgstr "Passé dans une nouvelle branche « %1 »"

#: fileviewgitplugin.cpp:585
#, kde-kuit-format
msgid ""
"<application>Git</application> Checkout failed. Maybe your working directory "
"is dirty."
msgstr ""
"L'extraction de <application>Git</application> a échoué. Il se peut que "
"votre dossier de travail ne soit pas à jour."

#: fileviewgitplugin.cpp:649
#, kde-kuit-format
msgid "Successfully created tag '%1'"
msgstr "Étiquette « %1 » créée avec succès"

#: fileviewgitplugin.cpp:654
#, kde-kuit-format
msgid ""
"<application>Git</application> tag creation failed. A tag with the name '%1' "
"already exists."
msgstr ""
"La création de l'étiquette dans <application>Git</application> a échoué. Une "
"étiquette portant le nom « %1 » existe déjà."

#: fileviewgitplugin.cpp:656
#, kde-kuit-format
msgid "<application>Git</application> tag creation failed."
msgstr ""
"La création de l'étiquette dans <application>Git</application> a échoué."

#: fileviewgitplugin.cpp:668
#, kde-kuit-format
msgid "Pushing branch %1 to %2:%3 failed."
msgstr "L'insertion de la branche %1 dans %2 : %3 a échoué."

#: fileviewgitplugin.cpp:670
#, kde-kuit-format
msgid "Pushed branch %1 to %2:%3."
msgstr "Branche %1 insérée dans %2 : %3."

#: fileviewgitplugin.cpp:672
#, kde-kuit-format
msgid "Pushing branch %1 to %2:%3..."
msgstr "Insertion de la branche %1 dans %2 : %3..."

#: fileviewgitplugin.cpp:694
#, kde-kuit-format
msgid "Pulling branch %1 from %2 failed."
msgstr "L'importation de la branche %1 depuis %2 a échoué."

#: fileviewgitplugin.cpp:696
#, kde-kuit-format
msgid "Pulled branch %1 from %2 successfully."
msgstr "Branche %1 importée depuis %2 avec succès."

#: fileviewgitplugin.cpp:698
#, kde-kuit-format
msgid "Pulling branch %1 from %2..."
msgstr "Importation de la branche %1 depuis %2..."

#: fileviewgitplugin.cpp:751 fileviewgitplugin.cpp:763
#, kde-kuit-format
msgid "Branch is already up-to-date."
msgstr "La branche est déjà à jour."

#: fileviewgitplugin.cpp:767
#, kde-kuit-format
msgid "Merge conflicts occurred. Fix them and commit the result."
msgstr ""
"Des conflits de fusion sont survenus. Corrigez-les et validez le résultat."

#. i18n: ectx: label, entry (commitDialogHeight), group (CommitDialogSettings)
#: fileviewgitpluginsettings.kcfg:7
#, kde-format
msgid "Dialog height"
msgstr "Hauteur de la boîte de dialogue"

#. i18n: ectx: label, entry (commitDialogWidth), group (CommitDialogSettings)
#: fileviewgitpluginsettings.kcfg:12
#, kde-format
msgid "Dialog width"
msgstr "Largeur de la boîte de dialogue"

#: pulldialog.cpp:24
#, kde-kuit-format
msgctxt "@title:window"
msgid "<application>Git</application> Pull"
msgstr "Importation dans <application>Git</application>"

#: pulldialog.cpp:35
#, kde-format
msgctxt "@action:button"
msgid "Pull"
msgstr "Importation"

#: pulldialog.cpp:44
#, kde-format
msgctxt "@title:group The source to pull from"
msgid "Source"
msgstr "Source"

#: pulldialog.cpp:50 pushdialog.cpp:51
#, kde-format
msgctxt "@label:listbox a git remote"
msgid "Remote:"
msgstr "Distante :"

#: pulldialog.cpp:55
#, kde-format
msgctxt "@label:listbox"
msgid "Remote branch:"
msgstr "Branche distante :"

#: pushdialog.cpp:25
#, kde-kuit-format
msgctxt "@title:window"
msgid "<application>Git</application> Push"
msgstr "Insertion dans <application>Git</application>"

#: pushdialog.cpp:37
#, kde-format
msgctxt "@action:button"
msgid "Push"
msgstr "Insertion"

#: pushdialog.cpp:47
#, kde-format
msgctxt "@title:group The remote host"
msgid "Destination"
msgstr "Destination"

#: pushdialog.cpp:61
#, kde-format
msgctxt "@title:group"
msgid "Branches"
msgstr "Branches"

#: pushdialog.cpp:65
#, kde-format
msgctxt "@label:listbox"
msgid "Local Branch:"
msgstr "Branche locale :"

#: pushdialog.cpp:72
#, kde-format
msgctxt "@label:listbox"
msgid "Remote Branch:"
msgstr "Branche distante :"

#: pushdialog.cpp:84
#, kde-format
msgctxt "@info:tooltip"
msgid ""
"Proceed even if the remote branch is not an ancestor of the local branch."
msgstr ""
"Continue même si la branche distante n'est pas un ancêtre de la branche "
"locale."

#: tagdialog.cpp:30
#, kde-kuit-format
msgctxt "@title:window"
msgid "<application>Git</application> Create Tag"
msgstr "Création d'une étiquette dans <application>Git</application>"

#: tagdialog.cpp:42
#, kde-format
msgctxt "@action:button"
msgid "Create Tag"
msgstr "Création d'étiquette"

#: tagdialog.cpp:51
#, kde-format
msgctxt "@title:group"
msgid "Tag Information"
msgstr "Informations sur l'étiquette"

#: tagdialog.cpp:55
#, kde-format
msgctxt "@label:textbox"
msgid "Tag Name:"
msgstr "Nom de l'étiquette :"

#: tagdialog.cpp:63
#, kde-format
msgctxt "@label:textbox"
msgid "Tag Message:"
msgstr "Message de l'étiquette :"

#: tagdialog.cpp:74
#, kde-format
msgctxt "@title:group"
msgid "Attach to"
msgstr "Joindre à"

#: tagdialog.cpp:81
#, kde-format
msgctxt "@label:listbox"
msgid "Branch:"
msgstr "Branche :"

#: tagdialog.cpp:127
#, kde-format
msgctxt "@info:tooltip"
msgid "You must enter a tag name first."
msgstr "Vous devez d'abord saisir un nom d'étiquette."

#: tagdialog.cpp:130
#, kde-format
msgctxt "@info:tooltip"
msgid "Tag names may not contain any whitespace."
msgstr "Il est impossible d'avoir des blancs dans les noms de branches."

#: tagdialog.cpp:133
#, kde-format
msgctxt "@info:tooltip"
msgid "A tag named '%1' already exists."
msgstr "Une étiquette nommée « %1 » existe déjà."
