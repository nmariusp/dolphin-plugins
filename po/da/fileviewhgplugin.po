# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Martin Schlander <mschlander@opensuse.org>, 2012, 2015, 2016, 2020.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-09 01:39+0000\n"
"PO-Revision-Date: 2020-07-27 22:07+0200\n"
"Last-Translator: Martin Schlander <mschlander@opensuse.org>\n"
"Language-Team: Danish <dansk@dansk-gruppen.dk>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 20.04.2\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: backoutdialog.cpp:31
#, kde-kuit-format
msgctxt "@title:window"
msgid "<application>Hg</application> Backout"
msgstr "<application>Hg</application> backout"

#: backoutdialog.cpp:33
#, kde-kuit-format
msgctxt "@action:button"
msgid "Backout"
msgstr "Backout"

#: backoutdialog.cpp:60
#, kde-kuit-format
msgctxt "@label:checkbox"
msgid "Merge with old dirstate parent after backout"
msgstr "Sammenfelt med gammel mappetilstand-forælder efter backout"

#: backoutdialog.cpp:62 backoutdialog.cpp:64 bundledialog.cpp:57
#, kde-kuit-format
msgctxt "@label:button"
msgid "Select Changeset"
msgstr "Vælg ændringssæt"

#: backoutdialog.cpp:68
#, kde-kuit-format
msgctxt "@label"
msgid "Revision to Backout: "
msgstr "Revision til backout: "

#: backoutdialog.cpp:73
#, kde-kuit-format
msgctxt "@label"
msgid "Parent Revision (optional): "
msgstr "Forælderrevision (valgfrit): "

#: backoutdialog.cpp:139 bundledialog.cpp:189
#, kde-kuit-format
msgctxt "@title:window"
msgid "Select Changeset"
msgstr "Vælg ændringssæt"

#: backoutdialog.cpp:140 bundledialog.cpp:190
#, kde-kuit-format
msgctxt "@action:button"
msgid "Select"
msgstr "Vælg"

#: branchdialog.cpp:24
#, kde-format
msgctxt "@title:window"
msgid "<application>Hg</application> Branch"
msgstr "<application>Hg</application> branch"

#: branchdialog.cpp:37
#, kde-format
msgid "Create New Branch"
msgstr "Opret "

#: branchdialog.cpp:38
#, kde-format
msgid "Switch Branch"
msgstr "Skift branch"

#: branchdialog.cpp:70
#, kde-format
msgid "<b>Current Branch: </b>"
msgstr "<b>Aktuel branch: </b>"

#: branchdialog.cpp:108 branchdialog.cpp:123 tagdialog.cpp:102
#: tagdialog.cpp:118 tagdialog.cpp:133
#, kde-format
msgid "Some error occurred"
msgstr "En fejl opstod"

#: bundledialog.cpp:31
#, kde-kuit-format
msgctxt "@title:window"
msgid "<application>Hg</application> Bundle"
msgstr "<application>Hg</application> bundle"

#: bundledialog.cpp:32
#, kde-kuit-format
msgctxt "@action:button"
msgid "Bundle"
msgstr "Bundle"

#: bundledialog.cpp:59
#, kde-kuit-format
msgctxt "@label"
msgid "Base Revision (optional): "
msgstr "Basisrevision (valgfrit): "

#: bundledialog.cpp:61
#, kde-kuit-format
msgctxt "@label"
msgid "Bundle all changesets in repository."
msgstr "Sammenføj alle ændringssæt i depotet."

#: bundledialog.cpp:76 exportdialog.cpp:55 importdialog.cpp:61
#: pulldialog.cpp:44 pushdialog.cpp:44
#, kde-format, kde-kuit-format
msgctxt "@label:group"
msgid "Options"
msgstr "Tilvalg"

#: bundledialog.cpp:78
#, kde-kuit-format
msgctxt "@label:checkbox"
msgid "Run even when the destination is unrelated (force)"
msgstr "Kør selv når destination ikke er relateret (force)"

#: bundledialog.cpp:81 pulldialog.cpp:40 pushdialog.cpp:40
#, kde-kuit-format
msgctxt "@label:checkbox"
msgid "Do not verify server certificate"
msgstr "Verificér ikke servercertifikat"

#: clonedialog.cpp:33
#, kde-kuit-format
msgctxt "@title:window"
msgid "<application>Hg</application> Clone"
msgstr "<application>Hg</application> clone"

#: clonedialog.cpp:34
#, kde-kuit-format
msgctxt "@action:button"
msgid "Clone"
msgstr "Klon"

#: clonedialog.cpp:42
#, kde-format
msgid "URLs"
msgstr "URL'er"

#: clonedialog.cpp:44
#, kde-kuit-format
msgctxt "@label"
msgid "Source"
msgstr "Kilde"

#: clonedialog.cpp:45
#, kde-kuit-format
msgctxt "@lobel"
msgid "Destination"
msgstr "Destination"

#: clonedialog.cpp:46 clonedialog.cpp:47
#, kde-kuit-format
msgctxt "@button"
msgid "Browse"
msgstr "Gennemse"

#: clonedialog.cpp:59
#, kde-format
msgctxt "@label"
msgid "Options"
msgstr "Tilvalg"

#: clonedialog.cpp:62
#, kde-format
msgid "Do not update the new working directory."
msgstr "Opdatér ikke den ny arbejdsmappe."

#: clonedialog.cpp:63
#, kde-format
msgid "Use pull protocol to copy metadata."
msgstr "Brug pull-protokollen til at kopiere metadata."

#: clonedialog.cpp:64
#, kde-format
msgid "Use uncompressed transfer."
msgstr "Brug ukomprimeret overførsel."

#: clonedialog.cpp:65
#, kde-format
msgid "Do not verify server certificate (ignoring web.cacerts config)."
msgstr ""
"Verificér ikke servercertifikatet (ignorerer web.cacerts-konfiguration)."

#: clonedialog.cpp:158
#, kde-format
msgid "Terminating cloning!"
msgstr "Afslutter kloning!"

#: clonedialog.cpp:184
#, kde-kuit-format
msgctxt "@action:button"
msgid "Close"
msgstr "Luk"

#: clonedialog.cpp:189
#, kde-kuit-format
msgctxt "@message:error"
msgid "Error Cloning Repository!"
msgstr "Fejl under kloning af depot!"

#: commitdialog.cpp:32
#, kde-kuit-format
msgctxt "@title:window"
msgid "<application>Hg</application> Commit"
msgstr "<application>Hg</application> commit"

#: commitdialog.cpp:34
#, kde-kuit-format
msgctxt "@action:button"
msgid "Commit"
msgstr "Commit"

#: commitdialog.cpp:41
#, kde-format
msgid ""
"The KTextEditor component could not be found;\n"
"please check your KDE Frameworks installation."
msgstr ""
"KDE teksteditor-komponenten kunne ikke findes.\n"
"Tjek din installation af KDE Frameworks."

#: commitdialog.cpp:54
#, kde-kuit-format
msgctxt "@action:inmenu"
msgid "Commit to current branch"
msgstr "Commit til aktuel branch"

#: commitdialog.cpp:59
#, kde-kuit-format
msgctxt "@action:inmenu"
msgid "Create new branch"
msgstr "Opret ny branch"

#: commitdialog.cpp:64
#, kde-kuit-format
msgctxt "@action:inmenu"
msgid "Close current branch"
msgstr "Luk aktuel branch"

#: commitdialog.cpp:86
#, kde-format
msgid "Copy Message"
msgstr "Kopi-besked"

#: commitdialog.cpp:87 updatedialog.cpp:34
#, kde-format
msgid "Branch"
msgstr "Branch"

#: commitdialog.cpp:108
#, kde-kuit-format
msgctxt "@title:group"
msgid "Commit Message"
msgstr "Commit-besked"

#: commitdialog.cpp:115
#, kde-kuit-format
msgctxt "@title:group"
msgid "Diff/Content"
msgstr "Diff/indhold"

#: commitdialog.cpp:217
#, kde-format
msgid "Could not create branch! Aborting commit!"
msgstr "Kunne ikke oprette branch! Afbryder commit!"

#: commitdialog.cpp:227
#, kde-format
msgid "Commit unsuccessful!"
msgstr "Commit mislykkedes!"

#: commitdialog.cpp:231
#, kde-format
msgid "No files for commit!"
msgstr "Ingen filer til commit!"

#: commitdialog.cpp:258
#, kde-format
msgid "Branch: Current Branch"
msgstr "Branch: Aktuel branch"

#: commitdialog.cpp:265
#, kde-format
msgid "Branch: "
msgstr "Branch: "

#: commitdialog.cpp:278
#, kde-format
msgid "Branch: Close Current"
msgstr "Branch: Luk aktuel"

#: commitdialog.cpp:291
#, kde-kuit-format
msgctxt "@title:window"
msgid "<application>Hg</application> Commit: New Branch"
msgstr "<application>Hg</application> commit: Ny branch"

#: commitdialog.cpp:299
#, kde-kuit-format
msgctxt "@label"
msgid "Enter new branch name"
msgstr "Angiv navn på ny branch"

#: commitdialog.cpp:320
#, kde-kuit-format
msgctxt "@label"
msgid "<b>Branch already exists!</b>"
msgstr "<b>Branchen findes allerede!</b>"

#: commitdialog.cpp:328
#, kde-kuit-format
msgctxt "@label"
msgid "<b>Enter some text!</b>"
msgstr "<b>Skriv noget tekst!</b>"

#: commitinfowidget.cpp:38
#, kde-format
msgid ""
"A KDE text-editor component could not be found;\n"
"please check your KDE installation."
msgstr ""
"Der blev ikke fundet en KDE teksteditor-komponent.\n"
"Tjek din KDE-installation."

#: configdialog.cpp:28
#, kde-kuit-format
msgctxt "@title:window"
msgid "<application>Hg</application> Repository Configuration"
msgstr "<application>Hg</application> depotkonfiguration"

#: configdialog.cpp:31
#, kde-kuit-format
msgctxt "@title:window"
msgid "<application>Hg</application> Global Configuration"
msgstr "<application>Hg</application> global konfiguration"

#: configdialog.cpp:44
#, kde-kuit-format
msgctxt "@label:group"
msgid "General Settings"
msgstr "Generelle indstillinger"

#: configdialog.cpp:48
#, kde-kuit-format
msgctxt "@label:group"
msgid "Repository Paths"
msgstr "Stier til depoter"

#: configdialog.cpp:51
#, kde-kuit-format
msgctxt "@label:group"
msgid "Ignored Files"
msgstr "Ignorerede filer"

#: configdialog.cpp:55
#, kde-kuit-format
msgctxt "@label:group"
msgid "Plugin Settings"
msgstr "Plugin-indstillinger"

#: createdialog.cpp:23
#, kde-kuit-format
msgctxt "@title:window"
msgid "<application>Hg</application> Initialize Repository"
msgstr "<application>Hg</application> Initialisér depot"

#: createdialog.cpp:24
#, kde-kuit-format
msgctxt "@action:button"
msgid "Initialize Repository"
msgstr "Initialisér depot"

#: createdialog.cpp:59
#, kde-kuit-format
msgctxt "error message"
msgid "Error creating repository!"
msgstr "Fejl under oprettelse af depot!"

#: exportdialog.cpp:28
#, kde-format
msgctxt "@title:window"
msgid "<application>Hg</application> Export"
msgstr "<application>Hg</application> export"

#: exportdialog.cpp:29
#, kde-kuit-format
msgctxt "@action:button"
msgid "Export"
msgstr "Eksportér"

#: exportdialog.cpp:56
#, kde-format
msgctxt "@label"
msgid "Treat all files as text"
msgstr "Behandl alle filer som tekst"

#: exportdialog.cpp:57
#, kde-format
msgctxt "@label"
msgid "Use Git extended diff format"
msgstr "Brug Git-udvidet diff-format"

#: exportdialog.cpp:58
#, kde-format
msgctxt "@label"
msgid "Omit dates from diff headers"
msgstr "Undtag datoer fra diff-headers"

#: exportdialog.cpp:127
#, kde-format
msgctxt "@message:error"
msgid "Please select at least one changeset to be exported!"
msgstr "Vælg mindst ét ændringssæt til eksport!"

#: fileviewhgplugin.cpp:94
#, kde-kuit-format
msgctxt "@action:inmenu"
msgid "<application>Hg</application> Add"
msgstr "<application>Hg</application> add"

#: fileviewhgplugin.cpp:101
#, kde-kuit-format
msgctxt "@action:inmenu"
msgid "<application>Hg</application> Remove"
msgstr "<application>Hg</application> remove"

#: fileviewhgplugin.cpp:108
#, kde-kuit-format
msgctxt "@action:inmenu"
msgid "<application>Hg</application> Rename"
msgstr "<application>Hg</application> rename"

#: fileviewhgplugin.cpp:115
#, kde-kuit-format
msgctxt "@action:inmenu"
msgid "<application>Hg</application> Commit"
msgstr "<application>Hg</application> commit"

#: fileviewhgplugin.cpp:122
#, kde-kuit-format
msgctxt "@action:inmenu"
msgid "<application>Hg</application> Tag"
msgstr "<application>Hg</application> tag"

#: fileviewhgplugin.cpp:129
#, kde-kuit-format
msgctxt "@action:inmenu"
msgid "<application>Hg</application> Branch"
msgstr "<application>Hg</application> branch"

#: fileviewhgplugin.cpp:136
#, kde-kuit-format
msgctxt "@action:inmenu"
msgid "<application>Hg</application> Clone"
msgstr "<application>Hg</application> clone"

#: fileviewhgplugin.cpp:143
#, kde-kuit-format
msgctxt "@action:inmenu"
msgid "<application>Hg</application> Init"
msgstr "<application>Hg</application> init"

#: fileviewhgplugin.cpp:150
#, kde-kuit-format
msgctxt "@action:inmenu"
msgid "<application>Hg</application> Update"
msgstr "<application>Hg</application> update"

#: fileviewhgplugin.cpp:157
#, kde-kuit-format
msgctxt "@action:inmenu"
msgid "<application>Hg</application> Global Config"
msgstr "<application>Hg</application> global konfiguration"

#: fileviewhgplugin.cpp:164
#, kde-kuit-format
msgctxt "@action:inmenu"
msgid "<application>Hg</application> Repository Config"
msgstr "<application>Hg</application> depotkonfiguration"

#: fileviewhgplugin.cpp:171
#, kde-kuit-format
msgctxt "@action:inmenu"
msgid "<application>Hg</application> Push"
msgstr "<application>Hg</application> push"

#: fileviewhgplugin.cpp:178
#, kde-kuit-format
msgctxt "@action:inmenu"
msgid "<application>Hg</application> Pull"
msgstr "<application>Hg</application> pull"

#: fileviewhgplugin.cpp:185
#, kde-kuit-format
msgctxt "@action:inmenu"
msgid "<application>Hg</application> Revert"
msgstr "<application>Hg</application> revert"

#: fileviewhgplugin.cpp:192
#, kde-kuit-format
msgctxt "@action:inmenu"
msgid "<application>Hg</application> Revert All"
msgstr "<application>Hg</application> revert all"

#: fileviewhgplugin.cpp:199
#, kde-kuit-format
msgctxt "@action:inmenu"
msgid "<application>Hg</application> Rollback"
msgstr "<application>Hg</application> rollback"

#: fileviewhgplugin.cpp:206
#, kde-kuit-format
msgctxt "@action:inmenu"
msgid "<application>Hg</application> Merge"
msgstr "<application>Hg</application> merge"

#: fileviewhgplugin.cpp:213
#, kde-kuit-format
msgctxt "@action:inmenu"
msgid "<application>Hg</application> Bundle"
msgstr "<application>Hg</application> bundle"

#: fileviewhgplugin.cpp:220
#, kde-kuit-format
msgctxt "@action:inmenu"
msgid "<application>Hg</application> Export"
msgstr "<application>Hg</application> export"

#: fileviewhgplugin.cpp:227
#, kde-kuit-format
msgctxt "@action:inmenu"
msgid "<application>Hg</application> Import"
msgstr "<application>Hg</application> import"

#: fileviewhgplugin.cpp:234
#, kde-kuit-format
msgctxt "@action:inmenu"
msgid "<application>Hg</application> Unbundle"
msgstr "<application>Hg</application> unbundle"

#: fileviewhgplugin.cpp:241
#, kde-kuit-format
msgctxt "@action:inmenu"
msgid "<application>Hg</application> Serve"
msgstr "<application>Hg</application> serve"

#: fileviewhgplugin.cpp:248
#, kde-kuit-format
msgctxt "@action:inmenu"
msgid "<application>Hg</application> Backout"
msgstr "<application>Hg</application> backout"

#: fileviewhgplugin.cpp:255
#, kde-kuit-format
msgctxt "@action:inmenu"
msgid "<application>Hg</application> Diff"
msgstr "<application>Hg</application> diff"

#: fileviewhgplugin.cpp:279
#, kde-kuit-format
msgctxt "@action:inmenu"
msgid "<application>Mercurial</application>"
msgstr "<application>Mercurial</application>"

#: fileviewhgplugin.cpp:489
#, kde-kuit-format
msgctxt "@info:status"
msgid "Adding files to <application>Hg</application> repository..."
msgstr "Føjer filer til <application>Hg</application>-depot..."

#: fileviewhgplugin.cpp:491
#, kde-kuit-format
msgctxt "@info:status"
msgid "Adding files to <application>Hg</application> repository failed."
msgstr ""
"Tilføjelse af filer til <application>Hg</application>-depot mislykkedes."

#: fileviewhgplugin.cpp:493
#, kde-kuit-format
msgctxt "@info:status"
msgid "Added files to <application>Hg</application> repository."
msgstr "Føjede filerne til <application>Hg</application>-depotet."

#: fileviewhgplugin.cpp:506
#, kde-kuit-format
msgctxt "@message:yesorno"
msgid "Would you like to remove selected files from the repository?"
msgstr "Vil du fjerne de valgte filer fra depotet?"

#: fileviewhgplugin.cpp:507
#, fuzzy, kde-format
#| msgctxt "@label:button"
#| msgid "Remove Patches"
msgid "Remove Files"
msgstr "Fjern rettelser"

#: fileviewhgplugin.cpp:513
#, kde-kuit-format
msgctxt "@info:status"
msgid "Removing files from <application>Hg</application> repository..."
msgstr "Fjerner filer fra <application>Hg</application>-depotet..."

#: fileviewhgplugin.cpp:515
#, kde-kuit-format
msgctxt "@info:status"
msgid "Removing files from <application>Hg</application> repository failed."
msgstr ""
"Fjernelse af filer fra <application>Hg</application>-depotet mislykkedes."

#: fileviewhgplugin.cpp:517
#, kde-kuit-format
msgctxt "@info:status"
msgid "Removed files from <application>Hg</application> repository."
msgstr "Fjernede filerne fra <application>Hg</application>-depotet."

#: fileviewhgplugin.cpp:529
#, kde-kuit-format
msgctxt "@info:status"
msgid "Renaming of file in <application>Hg</application> repository failed."
msgstr "Omdøbning af filer i <application>Hg</application>-depot mislykkedes."

#: fileviewhgplugin.cpp:531
#, kde-kuit-format
msgctxt "@info:status"
msgid "Renamed file in <application>Hg</application> repository successfully."
msgstr "Omdøbning af fil i <application>Hg</application>-depot gennemført."

#: fileviewhgplugin.cpp:533
#, kde-kuit-format
msgctxt "@info:status"
msgid "Renaming file in <application>Hg</application> repository."
msgstr "Omdøber fil i <application>Hg</application>-depot."

#: fileviewhgplugin.cpp:544
#, kde-kuit-format
msgctxt "@message"
msgid "No changes for commit!"
msgstr "Ingen ændringer til commit!"

#: fileviewhgplugin.cpp:549
#, kde-kuit-format
msgctxt "@info:status"
msgid "Commit to <application>Hg</application> repository failed."
msgstr "Commit til <application>Hg</application>-depot mislykkedes."

#: fileviewhgplugin.cpp:551
#, kde-kuit-format
msgctxt "@info:status"
msgid "Committed to <application>Hg</application> repository."
msgstr "Committede til <application>Hg</application>-depotet."

#: fileviewhgplugin.cpp:553
#, kde-kuit-format
msgctxt "@info:status"
msgid "Commit <application>Hg</application> repository."
msgstr "Commit <application>Hg</application>-depot."

#: fileviewhgplugin.cpp:564
#, kde-kuit-format
msgctxt "@info:status"
msgid "Tag operation in <application>Hg</application> repository failed."
msgstr "Tag-handling i <application>Hg</application>-depot mislykkedes."

#: fileviewhgplugin.cpp:566
#, kde-kuit-format
msgctxt "@info:status"
msgid ""
"Tagging operation in <application>Hg</application> repository is successful."
msgstr "Tag-handling i <application>Hg</application>-depot gennemført."

#: fileviewhgplugin.cpp:568
#, kde-kuit-format
msgctxt "@info:status"
msgid "Tagging operation in <application>Hg</application> repository."
msgstr "Tag-handling i <application>Hg</application>-depot."

#: fileviewhgplugin.cpp:577
#, kde-kuit-format
msgctxt "@info:status"
msgid "Update of <application>Hg</application> working directory failed."
msgstr "Opdatering af <application>Hg</application>-arbejdsmappe mislykkedes."

#: fileviewhgplugin.cpp:579
#, kde-kuit-format
msgctxt "@info:status"
msgid ""
"Update of <application>Hg</application> working directory is successful."
msgstr "Opdatering af <application>Hg</application>-arbejdsmappe gennemført."

#: fileviewhgplugin.cpp:581
#, kde-kuit-format
msgctxt "@info:status"
msgid "Updating <application>Hg</application> working directory."
msgstr "Opdaterer <application>Hg</application>-arbejdsmappe."

#: fileviewhgplugin.cpp:590
#, kde-kuit-format
msgctxt "@info:status"
msgid "Branch operation on <application>Hg</application> repository failed."
msgstr "Branch-handling på <application>Hg</application>-depot mislykkedes."

#: fileviewhgplugin.cpp:592
#, kde-kuit-format
msgctxt "@info:status"
msgid ""
"Branch operation on <application>Hg</application> repository completed "
"successfully."
msgstr "Branch-handling på <application>Hg</application>-depot gennemført."

#: fileviewhgplugin.cpp:594
#, kde-kuit-format
msgctxt "@info:status"
msgid "Branch operation on <application>Hg</application> repository."
msgstr "Branch-handling på <application>Hg</application>-depot."

#: fileviewhgplugin.cpp:692
#, kde-kuit-format
msgctxt "@message:yesorno"
msgid "Would you like to revert changes made to selected files?"
msgstr "Vil du fortryde ændringerne i de valgte filer?"

#: fileviewhgplugin.cpp:693 fileviewhgplugin.cpp:714
#, kde-format
msgid "Revert"
msgstr ""

#: fileviewhgplugin.cpp:699 fileviewhgplugin.cpp:720
#, kde-kuit-format
msgctxt "@info:status"
msgid "Reverting files in <application>Hg</application> repository..."
msgstr "Fortryder ændringer i filer i <application>Hg</application>-depot..."

#: fileviewhgplugin.cpp:701 fileviewhgplugin.cpp:722
#, kde-kuit-format
msgctxt "@info:status"
msgid "Reverting files in <application>Hg</application> repository failed."
msgstr ""
"Fortrydelse af ændringer af filer i <application>Hg</application>-depot "
"mislykkedes."

#: fileviewhgplugin.cpp:703 fileviewhgplugin.cpp:724
#, kde-kuit-format
msgctxt "@info:status"
msgid ""
"Reverting files in <application>Hg</application> repository completed "
"successfully."
msgstr ""
"Fortrydelse af ændringer af filer i <application>Hg</application>-depot "
"gennemført."

#: fileviewhgplugin.cpp:713
#, kde-kuit-format
msgctxt "@message:yesorno"
msgid "Would you like to revert all changes made to current working directory?"
msgstr "Vil du fortryde alle ændringer i den aktuelle arbejdsmappe?"

#: fileviewhgplugin.cpp:714
#, kde-format
msgid "Revert All"
msgstr ""

#: fileviewhgplugin.cpp:733
#, kde-kuit-format
msgctxt "@info:status"
msgid "Generating diff for <application>Hg</application> repository..."
msgstr "Genererer diff for <application>Hg</application>-depot..."

#: fileviewhgplugin.cpp:735
#, kde-kuit-format
msgctxt "@info:status"
msgid "Could not get <application>Hg</application> repository diff."
msgstr "Kunne ikke hente diff for <application>Hg</application>-depot."

#: fileviewhgplugin.cpp:737
#, kde-kuit-format
msgctxt "@info:status"
msgid "Generated <application>Hg</application> diff successfully."
msgstr "Generering af <application>Hg</application>-diff gennemført."

#: fileviewhgplugin.cpp:767
#, kde-kuit-format
msgctxt "@message:error"
msgid "abort: Uncommitted changes in working directory!"
msgstr "afbryd: Ikke-committede ændringer i arbejdsmappen!"

#: fileviewhgplugin.cpp:780
#, kde-kuit-format
msgctxt "@info:message"
msgid "No rollback information available!"
msgstr "Ingen tilbagerulningsinformation tilgængelig!"

#: fileviewhgplugin.cpp:792
#, kde-kuit-format
msgctxt "@message:yesorno"
msgid "Would you like to rollback last transaction?"
msgstr "Vil du rulle den seneste transaktion tilbage?"

#: fileviewhgplugin.cpp:793
#, kde-format
msgid "Rollback"
msgstr ""

#: fileviewhgplugin.cpp:799
#, kde-kuit-format
msgctxt "@info:status"
msgid "Executing Rollback <application>Hg</application> repository..."
msgstr "Udfører tilbagerulning af <application>Hg</application>-depot..."

#: fileviewhgplugin.cpp:801
#, kde-kuit-format
msgctxt "@info:status"
msgid "Rollback of <application>Hg</application> repository failed."
msgstr "Tilbagerulning af <application>Hg</application>-depot mislykkedes."

#: fileviewhgplugin.cpp:803
#, kde-kuit-format
msgctxt "@info:status"
msgid ""
"Rollback of <application>Hg</application> repository completed successfully."
msgstr "Tilbagerulning af <application>Hg</application>-depot gennemført."

#. i18n: ectx: label, entry (commitDialogHeight), group (CommitDialogSettings)
#. i18n: ectx: label, entry (configDialogHeight), group (ConfigDialogSettings)
#. i18n: ectx: label, entry (cloneDialogHeight), group (CloneDialogSettings)
#. i18n: ectx: label, entry (pushDialogBigHeight), group (PushDialogSettings)
#. i18n: ectx: label, entry (pullDialogBigHeight), group (PullDialogSettings)
#. i18n: ectx: label, entry (mergeDialogHeight), group (MergeDialogSettings)
#. i18n: ectx: label, entry (bundleDialogHeight), group (BundleDialogSettings)
#. i18n: ectx: label, entry (exportDialogHeight), group (ExportDialogSettings)
#. i18n: ectx: label, entry (importDialogHeight), group (ImportDialogSettings)
#. i18n: ectx: label, entry (serveDialogHeight), group (ServeDialogSettings)
#. i18n: ectx: label, entry (backoutDialogHeight), group (BackoutDialogSettings)
#: fileviewhgpluginsettings.kcfg:7 fileviewhgpluginsettings.kcfg:27
#: fileviewhgpluginsettings.kcfg:39 fileviewhgpluginsettings.kcfg:49
#: fileviewhgpluginsettings.kcfg:59 fileviewhgpluginsettings.kcfg:69
#: fileviewhgpluginsettings.kcfg:79 fileviewhgpluginsettings.kcfg:89
#: fileviewhgpluginsettings.kcfg:99 fileviewhgpluginsettings.kcfg:109
#: fileviewhgpluginsettings.kcfg:119
#, kde-format
msgid "Dialog height"
msgstr "Dialoghøjde"

#. i18n: ectx: label, entry (commitDialogWidth), group (CommitDialogSettings)
#. i18n: ectx: label, entry (configDialogWidth), group (ConfigDialogSettings)
#. i18n: ectx: label, entry (cloneDialogWidth), group (CloneDialogSettings)
#. i18n: ectx: label, entry (pushDialogBigWidth), group (PushDialogSettings)
#. i18n: ectx: label, entry (pullDialogBigWidth), group (PullDialogSettings)
#. i18n: ectx: label, entry (mergeDialogWidth), group (MergeDialogSettings)
#. i18n: ectx: label, entry (bundleDialogWidth), group (BundleDialogSettings)
#. i18n: ectx: label, entry (exportDialogWidth), group (ExportDialogSettings)
#. i18n: ectx: label, entry (importDialogWidth), group (ImportDialogSettings)
#. i18n: ectx: label, entry (serveDialogWidth), group (ServeDialogSettings)
#. i18n: ectx: label, entry (backoutDialogWidth), group (BackoutDialogSettings)
#: fileviewhgpluginsettings.kcfg:12 fileviewhgpluginsettings.kcfg:32
#: fileviewhgpluginsettings.kcfg:43 fileviewhgpluginsettings.kcfg:53
#: fileviewhgpluginsettings.kcfg:63 fileviewhgpluginsettings.kcfg:73
#: fileviewhgpluginsettings.kcfg:83 fileviewhgpluginsettings.kcfg:93
#: fileviewhgpluginsettings.kcfg:103 fileviewhgpluginsettings.kcfg:113
#: fileviewhgpluginsettings.kcfg:123
#, kde-format
msgid "Dialog width"
msgstr "Dialogbredde"

#. i18n: ectx: label, entry (verticalSplitterSizes), group (CommitDialogSettings)
#: fileviewhgpluginsettings.kcfg:17
#, kde-format
msgid "Divides file list and editors with commit details"
msgstr "Opdeler fillisten og editors med commit-detaljer"

#. i18n: ectx: label, entry (horizontalSplitterSizes), group (CommitDialogSettings)
#: fileviewhgpluginsettings.kcfg:21
#, kde-format
msgid "Divides commit editor and diff editor"
msgstr "Opdeler commit-editor og diff-editor"

#: importdialog.cpp:30
#, kde-kuit-format
msgctxt "@title:window"
msgid "<application>Hg</application> Import"
msgstr "<application>Hg</application> import"

#: importdialog.cpp:31
#, kde-kuit-format
msgctxt "@action:button"
msgid "Import"
msgstr "Importér"

#: importdialog.cpp:63
#, kde-kuit-format
msgctxt "@label"
msgid "Do not commit, just update the working directory"
msgstr "Commit ikke, opdatér blot arbejdsmappen"

#: importdialog.cpp:65
#, kde-kuit-format
msgctxt "@label"
msgid "Skip test for outstanding uncommitted changes"
msgstr "Skip test for udestående ikke-committede ændringer"

#: importdialog.cpp:67
#, kde-kuit-format
msgctxt "@label"
msgid "Apply patch to the nodes from which it was generated"
msgstr "Anvend rettelse på de knuder den blev genereret fra"

#: importdialog.cpp:69
#, kde-kuit-format
msgctxt "@label"
msgid "Apply patch without touching working directory"
msgstr "Anvend rettelse uden at røre arbejdsmappen"

#: importdialog.cpp:81
#, kde-kuit-format
msgctxt "@label:button"
msgid "Add Patches"
msgstr "Tilføj rettelser"

#: importdialog.cpp:83
#, kde-kuit-format
msgctxt "@label:button"
msgid "Remove Patches"
msgstr "Fjern rettelser"

#: mergedialog.cpp:27
#, kde-kuit-format
msgctxt "@title:window"
msgid "<application>Hg</application> Merge"
msgstr "<application>Hg</application> merge"

#: mergedialog.cpp:28
#, kde-kuit-format
msgctxt "@label:button"
msgid "Merge"
msgstr "Sammenflet"

#: mergedialog.cpp:108
#, kde-kuit-format
msgctxt "@message"
msgid "No head selected for merge!"
msgstr "Intet head valgt til sammenfletning!"

#: pathselector.cpp:60
#, kde-kuit-format
msgctxt "@label:combobox"
msgid "edit"
msgstr "redigér"

#: pulldialog.cpp:30
#, kde-kuit-format
msgctxt "@title:window"
msgid "<application>Hg</application> Pull Repository"
msgstr "<application>Hg</application> pull repository"

#: pulldialog.cpp:38
#, kde-kuit-format
msgctxt "@label:checkbox"
msgid "Update to new branch head if changesets were pulled"
msgstr "Opdatér til nyt branch-head hvis ændringssæt blev pulled"

#: pulldialog.cpp:42
#, kde-kuit-format
msgctxt "@label:checkbox"
msgid "Force Pull"
msgstr "Force pull"

#: pulldialog.cpp:54
#, kde-kuit-format
msgctxt "@label:group"
msgid "Incoming Changes"
msgstr "Indkommende ændringer"

#: pulldialog.cpp:148
#, kde-kuit-format
msgctxt "@message:info"
msgid "No incoming changes!"
msgstr "Ingen indkommende ændringer!"

#: pushdialog.cpp:30
#, kde-kuit-format
msgctxt "@title:window"
msgid "<application>Hg</application> Push Repository"
msgstr "<application>Hg</application> push repository"

#: pushdialog.cpp:38
#, kde-kuit-format
msgctxt "@label:checkbox"
msgid "Allow pushing a new branch"
msgstr "Tillad pushing af en ny branch"

#: pushdialog.cpp:42
#, kde-kuit-format
msgctxt "@label:checkbox"
msgid "Force Push"
msgstr "Force push"

#: pushdialog.cpp:54
#, kde-kuit-format
msgctxt "@label:group"
msgid "Outgoing Changes"
msgstr "Udgående ændringer"

#: pushdialog.cpp:170
#, kde-kuit-format
msgctxt "@message:info"
msgid "No outgoing changes!"
msgstr "Ingen udgående ændringer!"

#: renamedialog.cpp:25
#, kde-kuit-format
msgctxt "@title:window"
msgid "<application>Hg</application> Rename"
msgstr "<application>Hg</application> rename"

#: renamedialog.cpp:27
#, kde-kuit-format
msgctxt "@action:button"
msgid "Rename"
msgstr "Omdøb"

#: renamedialog.cpp:33
#, kde-kuit-format
msgctxt "@label:label to source file"
msgid "Source "
msgstr "Kilde "

#: renamedialog.cpp:39
#, kde-kuit-format
msgctxt "@label:rename"
msgid "Rename to "
msgstr "Omdøb til "

#: servedialog.cpp:25
#, kde-kuit-format
msgctxt "@title:window"
msgid "<application>Hg</application> Serve"
msgstr "<application>Hg</application> serve"

#: servedialog.cpp:63
#, kde-kuit-format
msgctxt "@label:button"
msgid "Start Server"
msgstr "Start server"

#: servedialog.cpp:64
#, kde-kuit-format
msgctxt "@label:button"
msgid "Stop Server"
msgstr "Stop server"

#: servedialog.cpp:65
#, kde-kuit-format
msgctxt "@label:button"
msgid "Open in browser"
msgstr "Åbn i browser"

#: servedialog.cpp:81
#, kde-kuit-format
msgctxt "@label"
msgid "Port"
msgstr "Port"

#: servewrapper.cpp:68
#, kde-format
msgid "## Starting Server ##"
msgstr "## Starter Server ##"

#: servewrapper.h:151
#, kde-format
msgid "## Server Stopped! ##\n"
msgstr ""

#: statuslist.cpp:30
#, kde-format
msgid "Filename"
msgstr "Filnavn"

#: statuslist.cpp:40
#, kde-format
msgctxt "@title:group"
msgid "File Status"
msgstr "Filstatus"

#: syncdialogbase.cpp:82
#, kde-format
msgctxt "@label:button"
msgid "Show Incoming Changes"
msgstr "Vis indkommende ændringer"

#: syncdialogbase.cpp:86
#, kde-format
msgctxt "@label:button"
msgid "Show Outgoing Changes"
msgstr "Vis udgående ændringer"

#: syncdialogbase.cpp:120
#, kde-format
msgctxt "@action:button"
msgid "Pull"
msgstr "Pull"

#: syncdialogbase.cpp:120
#, kde-format
msgctxt "@action:button"
msgid "Push"
msgstr "Push"

#: syncdialogbase.cpp:162
#, kde-format
msgctxt "@message"
msgid "No changes found!"
msgstr "Ingen ændringer fundet!"

#: syncdialogbase.cpp:213 syncdialogbase.cpp:288 syncdialogbase.cpp:295
#, kde-format
msgid "Error!"
msgstr "Fejl!"

#: syncdialogbase.cpp:329
#, kde-kuit-format
msgctxt "@action:button"
msgid "Options"
msgstr "Tilvalg"

#: tagdialog.cpp:20
#, kde-kuit-format
msgctxt "@title:window"
msgid "<application>Hg</application> Tag"
msgstr "<application>Hg</application> tag"

#: tagdialog.cpp:30
#, kde-format
msgid "Create New Tag"
msgstr "Opret nyt tag"

#: tagdialog.cpp:31
#, kde-format
msgid "Remove Tag"
msgstr "Fjern tag"

#: tagdialog.cpp:32
#, kde-format
msgid "Switch Tag"
msgstr "Skift tag"

#: tagdialog.cpp:129
#, kde-format
msgid "Created tag successfully!"
msgstr "Tag oprettet!"

#: updatedialog.cpp:25
#, kde-kuit-format
msgctxt "@title:window"
msgid "<application>Hg</application> Update"
msgstr "<application>Hg</application> update"

#: updatedialog.cpp:27
#, kde-kuit-format
msgctxt "@action:button"
msgid "Update"
msgstr "Opdatér"

#: updatedialog.cpp:30
#, kde-format
msgid "New working directory"
msgstr "Ny arbejdsmappe"

#: updatedialog.cpp:35
#, kde-format
msgid "Tag"
msgstr "Tag"

#: updatedialog.cpp:36
#, kde-format
msgid "Changeset/Revision"
msgstr "Ændringssæt/revision"

#: updatedialog.cpp:41
#, kde-format
msgid "Current Parent"
msgstr "Aktuel forælder"

#: updatedialog.cpp:47
#, kde-format
msgid "Options"
msgstr "Tilvalg"

#: updatedialog.cpp:49
#, kde-format
msgid "Discard uncommitted changes"
msgstr "Kassér ændringer som ikke er committet"

#: updatedialog.cpp:126
#, kde-format
msgid ""
"Some error occurred! \n"
"Maybe there are uncommitted changes."
msgstr ""
"En fejl opstod! \n"
"Måske er der ikke-committede ændringer."
